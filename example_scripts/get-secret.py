#!/usr/bin/env python3
import sys
import argparse
from azure.identity import DefaultAzureCredential
from azure.keyvault.secrets import SecretClient


def parse_args():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(
        description="This is my super sweet script")
    parser.add_argument("-v",
                        "--verbose",
                        help="Be verbose",
                        action="store_true",
                        dest="verbose")

    return parser.parse_args()


def main():
    args = parse_args()
    credential = DefaultAzureCredential()
    kv_url = "https://ssi-drew-test.vault.azure.net/"
    client = SecretClient(vault_url=kv_url, credential=credential)
    secret_name = "drew-super-secret"
    retrieved_secret = client.get_secret(secret_name)
    print(retrieved_secret.value)
    return 0


if __name__ == "__main__":
    sys.exit(main())
