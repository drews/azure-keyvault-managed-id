# Overview

Using a managed identity to access values in an Azure Keyvault

## Prereqs

### Managed Identity

Create a 'Managed Identity'.  This will be a special user stored in Azure.

### Key Vault

Create a Key Vault, and put some secrets in there.  Note that access policies
aren't as fine grained as HashiCorp Vault, keep this in mind when determining which secrets
should go in the Vault

#### Creating

When creating a vault, one of the options will be retention time.  This is for
retaining `deleted` secrets, so they can be restored if needed

Start with minimal access policies.  For example, the `Azure Virtual Machines
for deployment` checkbox does _not_ need to be checked to give Virtual Machines
access to secrets

#### Types of Secrets

* Keys - These are RSA or EC style keys, auto generated or imported

* Secrets - These are traditional key value keys (foo=bar), manually entered

* Certificates - TLS certs, auto generated or imported

#### Grant Access to Managed Identity

Under `Access Policies`, add a policy for your newly created managed identity.
As this is probably going to be used for automation, keep the permissions
minimal (`get` secrets, no need to `set` or `delete`, etc)

### VM Identity

Under `Identity` -> `User Assigned`, add your newly created Managed Identity

## Example Scripts

`example_scripts/get-secret.py` - Retrieve and print a secret from the vault,
using the managed identity machine credentials

## Useful Links

[Python Key Vault Scripting](https://docs.microsoft.com/en-us/azure/key-vault/secrets/quick-create-python)
